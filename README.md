Flaconi coding challenge
=========================

This project was built for Flaconi coding challenge. This documentation aims specifying how what this project contains.

*Requirements:*

To run this project, you should have the following programs:

* docker
* docker-compose

*What will be installed*

In the file `docker/docker-compose.yml` there is the specification of the stack used, that are:

* PHP:7.2-fpm
  * Extensions: zip, pdo, mongodb, xdebug
  * Composer
  * Symfony 4.1
    * Browser-kit
    * Maker-bundle
    * ODM
    * Test-pack
  * PHPUnit  
* MongoDB
* Nginx:latest

*How to run:*

To simplify the process, I created a shell script to setup and up the application: `./run.sh`. It will run internally
 a docker-compose up and install the dependencies.

You can kill the application with `docker-compose -f docker/docker-compose.yml down`

To run the tests, you may use the command `./tests.sh [option]`.
Where: [option] can be `coverage`. If no option is informed, it will run the entire test suite.

An openAPI-style documentation is placed on `Docs/swagger.api.yml`.
There is also some sample tests made on Postman on `Docs/Flaconi.postman_collection.json`

*Note: This service is running on the port `8000` accessing from outside the container. It may conflict with other 
service on the same port*