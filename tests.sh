#!/bin/bash
cp ./.env.dist ./.env
docker-compose -f docker/docker-compose.yml up -d --build

option=$1
case $option in
    coverage)
        command=tests-coverage
        ;;
    *)
        command=tests
        ;;
esac

docker-compose -f docker/docker-compose.yml exec php composer $command