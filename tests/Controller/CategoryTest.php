<?php

namespace Flaconi\Tests\Controller;

use Flaconi\Document\Category as CategoryDocument;
use Flaconi\Dto\Category;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryTest extends WebTestCase
{
    /**
     * @var \Symfony\Component\BrowserKit\Client
     */
    private $client;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $kernel = static::bootKernel();
        $documentManager = $kernel->getContainer()->get('doctrine_mongodb')->getManager();
        $documentManager->getDocumentCollection(CategoryDocument::class)->remove([]);
    }


    public function test_will_create_category()
    {
        $this->client->request(
            'POST',
            '/category',
            [],
            [],
            [],
            '
{
	"name": "Perfum"
}
'
        );

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $response = json_decode($this->client->getResponse()->getContent(), true);
        $dtoCategory = Category::fromArray($response);

        $this->client->request(
            'GET',
            '/category/',
            [
                'id' => $dtoCategory->getId()
            ]
        );

        $this->client->followRedirect();
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function test_will_create_no_category()
    {
        $this->client->request(
            'GET',
            '/category?id=invalid'
        );

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function test_wil_retrieve_category_tree()
    {
        $this->client->request(
            'POST',
            '/category',
            [],
            [],
            [],
            '
{
	"name": "Perfum"
}
'
        );

        $rootContent = json_decode($this->client->getResponse()->getContent(), true);
        $rootCategory = Category::fromArray($rootContent);

        $this->client->request(
            'POST',
            '/category',
            [],
            [],
            [],
            '
{
	"name": "Men Perfum",
	"parentCategorySlug": "perfum"
}
'
        );

        $this->client->request(
            'POST',
            '/category',
            [],
            [],
            [],
            '
{
	"name": "Wooden Men Perfum",
	"parentCategorySlug": "men-perfum"
}
'
        );

        $this->client->request(
            'PATCH',
            '/category/' . $rootCategory->getId() . '/status',
            [],
            [],
            [],
            '
{
	"isVisible": true
}            
            '
        );

        $this->client->request(
            'GET',
            '/category/' . $rootCategory->getId() . '/tree'
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $responseContent = json_decode($this->client->getResponse()->getContent(), true);
        $expected = [
            $rootCategory->getId() => [
                'name' => 'Perfum',
                'children' => []
            ]
        ];
        $this->assertEquals($expected, $responseContent);
    }
}