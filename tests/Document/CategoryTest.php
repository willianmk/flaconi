<?php

namespace Flaconi\Tests\Document;

use Flaconi\Document\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function test_will_create_category_document()
    {
        $id = '12345abc';
        $name = 'Name Test';
        $slug = 'name-test';

        $document = new Category();
        $document->setId($id);
        $document->setName($name);
        $document->setSlug($slug);
        $document->setIsVisible(false);
        $document->setParentCategory($document);

        $this->assertEquals($id, $document->getId());
        $this->assertEquals($name, $document->getName());
        $this->assertEquals($slug, $document->getSlug());
        $this->assertInstanceOf(Category::class, $document->getParentCategory());
        $this->assertFalse($document->isVisible());
    }
}