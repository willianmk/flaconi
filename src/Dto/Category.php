<?php

namespace Flaconi\Dto;

use Flaconi\Document\Category as CategoryDocument;

class Category
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $parentCategorySlug;

    /**
     * @var \Flaconi\Dto\Category
     */
    private $parentCategory;

    /**
     * @var bool
     */
    private $isVisible;

    /**
     * Category constructor.
     * @param null|string $id
     * @param string $name
     * @param string $slug
     * @param null|string $parentCategorySlug
     * @param \Flaconi\Dto\Category|null $parentCategory
     * @param bool|null $isVisible
     */
    private function __construct(
        ?string $id,
        string $name,
        string $slug,
        ?string $parentCategorySlug,
        ?Category $parentCategory,
        ?bool $isVisible
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
        $this->parentCategorySlug = $parentCategorySlug;
        $this->parentCategory = $parentCategory;
        $this->isVisible = $isVisible;
    }

    /**
     * @param array $data
     * @return \Flaconi\Dto\Category
     */
    public static function fromArray(array $data): Category
    {
        $id = $data['id'] ?? null;
        $name = $data['name'] ?? '';
        $slug = $data['slug'] ?? '';
        $parentCategorySlug = $data['parentCategorySlug'] ?? null;
        $parentCategory = isset($data['parentCategory']) ? self::fromArray($data['parentCategory']) : null;
        $isVisible = $data['isVisible'] ?? false;

        return new static(
            $id, $name, $slug, $parentCategorySlug, $parentCategory, $isVisible
        );
    }

    /**
     * @param \Flaconi\Document\Category $document
     * @return \Flaconi\Dto\Category
     */
    public static function fromDocument(CategoryDocument $document): Category
    {
        $parentCategoryId = $document->getParentCategory() ? $document->getParentCategory()->getId() : '';
        $parentCategory = $document->getParentCategory() ? static::fromDocument($document->getParentCategory()) : null;

        return new static(
            $document->getId(),
            $document->getName(),
            $document->getSlug(),
            $parentCategoryId,
            $parentCategory,
            $document->isVisible()
        );
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'parentCategory' => $this->parentCategory ? $this->parentCategory->toArray() : null,
            'isVisible' => $this->isVisible
        ];
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getParentCategorySlug(): ?string
    {
        return $this->parentCategorySlug;
    }

    /**
     * @return \Flaconi\Dto\Category
     */
    public function getParentCategory(): ?Category
    {
        return $this->parentCategory;
    }

    /**
     * @param \Flaconi\Dto\Category $parentCategory
     */
    public function setParentCategory(?Category $parentCategory): void
    {
        $this->parentCategory = $parentCategory;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible;
    }
}