<?php

namespace Flaconi\Infrastructure\Repository\Doctrine;

use Cocur\Slugify\Slugify;
use Doctrine\ODM\MongoDB\DocumentManager;
use Flaconi\Document\Category as CategoryDocument;
use Flaconi\Dto\Category as CategoryDto;
use Flaconi\Exception\CategoryNotFoundException;

class CategoryRepository
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    /**
     * @param \Flaconi\Dto\Category $categoryDto
     * @return \Flaconi\Dto\Category
     */
    public function create(CategoryDto $categoryDto)
    {
        $categoryDocument = new CategoryDocument();
        $categoryDocument->setName($categoryDto->getName());

        $slugify = new Slugify();
        $slug = $slugify->slugify($categoryDto->getName());
        $categoryDocument->setSlug($slugify->slugify($slug));
        $categoryDto->setSlug($slug);

        $this->setParentCategoryOn($categoryDocument, $categoryDto);

        $categoryDocument->setIsVisible(false);

        $this->documentManager->persist($categoryDocument);
        $this->documentManager->flush();

        return CategoryDto::fromDocument($categoryDocument);
    }

    /**
     * @param $id
     * @param $slug
     * @return \Flaconi\Document\Category|null
     * @throws \Flaconi\Exception\CategoryNotFoundException
     */
    public function searchCategoryByIdOrSlug($id, $slug): ?CategoryDocument
    {
        $result = $this->documentManager->find(CategoryDocument::class, $id);

        if ($result) {
            return $result;
        }

        $result = $this->documentManager
            ->getRepository(CategoryDocument::class)
            ->findOneBy(['slug' => $slug]);

        if ($result) {
            return $result;
        }

        throw new CategoryNotFoundException('Category not found');
    }

    /**
     * @param string $id
     * @return array
     * @throws \Flaconi\Exception\CategoryNotFoundException
     */
    public function generateTree(string $id): array
    {
        $tree = [];

        /**
         * @var Category
         */
        $category = $this->documentManager
            ->getRepository(CategoryDocument::class)
            ->findOneBy(
                [
                    'id' => $id,
                    'isVisible' => true
                ]
            );

        if (!$category) {
            throw new CategoryNotFoundException('Category not found: ' . $id);
        }

        $tree[$id] = [
            'name' => $category->getName()
        ];

        $tree[$id]['children'] = [];
        $tree[$id]['children'] = $this->getChildren($tree[$id]['children'], $category);

        return $tree;
    }

    /**
     * @param \Flaconi\Dto\Category $dto
     * @return \Flaconi\Dto\Category
     * @throws \Flaconi\Exception\CategoryNotFoundException
     */
    public function updateStatus(CategoryDto $dto): CategoryDto
    {
        $document = $this->documentManager->find(CategoryDocument::class, $dto->getId());

        if (!$document) {
            throw new CategoryNotFoundException();
        }

        $document->setIsVisible($dto->isVisible());

        $this->documentManager->persist($document);
        $this->documentManager->flush();

        return CategoryDto::fromDocument($document);
    }

    private function getChildren(array $tree, CategoryDocument $category): array
    {
        $children = $this->documentManager
            ->getRepository(CategoryDocument::class)
            ->findBy(
                [
                    'parentCategory.id' => $category->getId(),
                    'isVisible' => true
                ]
            );

        if ($children) {
            foreach ($children as $child) {
                $tree[$child->getId()]['name'] = $child->getName();
                $tree[$child->getId()]['children'] = [];
                $tree[$child->getId()]['children'] = $this->getChildren($tree[$child->getId()]['children'], $child);
            }
        }

        return $tree;
    }

    private function setParentCategoryOn(CategoryDocument $category, CategoryDto $categoryDto)
    {
        $parentCategory = $this->documentManager
            ->getRepository(CategoryDocument::class)
            ->findOneBy(
                [
                    'slug' => $categoryDto->getParentCategorySlug()
                ]
            );

        $category->setParentCategory($parentCategory);

        if ($parentCategory) {
            $categoryDto->setParentCategory(CategoryDto::fromDocument($parentCategory));
        }
    }
}