<?php

namespace Flaconi\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * @Document(db="flaconi", collection="category")
 */
class Category
{
    /**
     * @Id(strategy="UUID", type="string")
     * @var string
     */
    private $id;

    /**
     * @Field(type="string")
     * @var string
     */
    private $name;

    /**
     * @Field(type="string")
     * @var string
     */
    private $slug;

    /**
     * @EmbedOne(targetDocument="Flaconi\Document\Category")
     * @var \Flaconi\Document\Category
     */
    private $parentCategory;

    /**
     * @Field(type="boolean")
     * @var bool
     */
    private $isVisible;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return \Flaconi\Document\Category
     */
    public function getParentCategory(): ?Category
    {
        return $this->parentCategory;
    }

    /**
     * @param \Flaconi\Document\Category $parentCategory
     */
    public function setParentCategory(?Category $parentCategory): void
    {
        $this->parentCategory = $parentCategory;
    }

    /**
     * @return bool
     */
    public function isVisible(): ?bool
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     */
    public function setIsVisible(?bool $isVisible): void
    {
        $this->isVisible = $isVisible;
    }
}