<?php

namespace Flaconi\Controller;

use Flaconi\Dto\Category;
use Flaconi\Exception\CategoryNotFoundException;
use Flaconi\Infrastructure\Repository\Doctrine\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractController
{
    public function create(Request $request, CategoryRepository $repository)
    {
        $jsonContent = json_decode($request->getContent(), true);
        $categoryDto = Category::fromArray($jsonContent);

        try{
            $return = $repository->create($categoryDto);
        } catch (\Exception $e) {
            return $this->json([
                'error' => 'Bad format',
                'message' => $e->getMessage()
            ],
                400);
        }

        return $this->json($return->toArray(), 201);
    }

    public function search(Request $request, CategoryRepository $repository)
    {
        $id = $request->get('id', '');
        $slug = $request->get('slug', '');

        try {
            $result = $repository->searchCategoryByIdOrSlug($id, $slug);
            $response = Category::fromDocument($result);
        } catch (CategoryNotFoundException $e) {
            return $this->json(
                [
                    'error' => 'Not found',
                    'message' => 'Category not found for: id="' . $id . '", slug="' . $slug . '"'
                ],
                404
            );
        }

        return $this->json(
            $response->toArray()
        );
    }

    public function tree($id, CategoryRepository $repository)
    {
        try{
            $tree = $repository->generateTree($id);
        } catch(CategoryNotFoundException $e) {
            return $this->json(
                [
                    'error' => 'Not found',
                    'message' => 'Category not found. ID: ' . $id
                ],
                404
            );
        }

        return $this->json($tree, 200);
    }

    public function updateStatus($id, Request $request, CategoryRepository $repository)
    {
        $content = json_decode($request->getContent(), true);
        $content['id'] = $id;
        $categoryDto = Category::fromArray(
            $content
        );

        try{
            $response = $repository->updateStatus($categoryDto);
        } catch (CategoryNotFoundException $e) {
            return $this->json(
                [
                    'error' => 'Not found',
                    'message' => 'Category not found: ' . $id
                ],
                404
            );
        }

        return $this->json(
            $response->toArray(), 200
        );
    }
}